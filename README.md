# EmbededTraining
This is a training program for learning/teaching embeded from scratch. The guide assume you have no prior knowledge of the topic, so you can skip what you know.
## Guide
This is a guide, in latex, for learning embeded. It uses Arduino, since it is cheap and easy to use.
## Exercises
This contains code for the exercises. The first time a notion is seen (first led flash, first button press) the code is included, the next time, it is not.
## Solutions
This contains the solutions, at least a solution, for all exercises. It also include the wiring diagram. It should be useds as a teacher and not trainee, since it make it easy to skip the learning by trial phase.

# License and stuff
You can do what you want with this. If you use it, i would apreciate credits. If you make improvement and/or add challenges and exercises, please make a pull request so others can benefit from it.
